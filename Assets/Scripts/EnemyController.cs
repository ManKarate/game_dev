﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    Rigidbody2D rigidbody;
    float jump_timer;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.freezeRotation = true;
 
    }
	
	// Update is called once per frame
	void Update () {
        jump_timer -= Time.deltaTime;
        if (jump_timer<=0) {
            rigidbody.AddForce(new Vector2(0, 20), ForceMode2D.Impulse);
            jump_timer = 1;
        }
	}
}
