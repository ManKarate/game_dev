﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    bool knockback;
    bool facingRight;
    bool dashRight;

    bool hit;
    float hit_timer;

    float immune_timer;
    public float immune_timer_reset;

    public float hit_reset_timer;
    Vector2 hit_position;


    public float maxSpeed;
    public float jumpShortSpeed;
    public float jumpSpeed;

    bool jump = false;
    bool jump_cancel = false;
    bool can_multi_jump = false;

    bool dash;

    public float dash_time;
    float dash_timer;

    public float dash_cooldown;
    float dash_cooldown_timer;

    public float dash_speed_mult;

    public int max_jump_count;
    int jump_count;
    bool is_grounded;

    Transform groundCheck;
    Transform leftWallCheck;
    Transform rightWallCheck;

    float gravityScale = 5;

    bool leftWallTouch;
    bool rightWallTouch;

    float wallJumpTimer;

    bool wallJump;

    Rigidbody2D rigidbody;

    [HideInInspector]
    public BoxCollider2D collider;



    float jump_timer;



    // Use this for initialization
    public void Awake() {
        collider = GetComponent<BoxCollider2D>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Start() {
        hit = false;
        facingRight = true;
        hit_position = new Vector2();
        is_grounded = false;
        groundCheck = transform.Find("groundCheck");
        leftWallCheck = transform.Find("leftWallCheck");
        rightWallCheck = transform.Find("rightWallCheck");
        rigidbody.freezeRotation = true;
        knockback = false;
        jump_count = max_jump_count;
    }

    // Update is called once per frame
    void Update() {

        Debug.Log(immune_timer);

        if(immune_timer >0) {
            immune_timer -= Time.deltaTime;
        }
        //reset for falling off the map
        if (transform.position.y < -20) {
            transform.position = Vector3.zero;
        }

        is_grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Obstacle"));
        leftWallTouch = Physics2D.Linecast(transform.position, leftWallCheck.position, 1 << LayerMask.NameToLayer("Obstacle"));
        rightWallTouch = Physics2D.Linecast(transform.position, rightWallCheck.position, 1 << LayerMask.NameToLayer("Obstacle"));



        if (!is_grounded && leftWallTouch && Input.GetAxisRaw("Horizontal") < 0 || !is_grounded && rightWallTouch && Input.GetAxisRaw("Horizontal") > 0) {

            wallJump = true;


        }
        else
            wallJump = false;


        if (is_grounded) {
            jump_count = max_jump_count;
            can_multi_jump = false;

        }

        //update cooldown timers
        if (dash_cooldown_timer > 0) {
            dash_cooldown_timer -= Time.deltaTime;
        }

        if (!hit) {
            if (Input.GetButtonDown("Jump") && is_grounded) {
                jump = true;
                jump_count--;
            }

            if (Input.GetButtonDown("Jump") && rigidbody.velocity.y <= 0 && !is_grounded && jump_count > 0 && can_multi_jump) {
                jump_count--;
                jump = true;
            }

            if (Input.GetButtonUp("Jump") && !is_grounded) {
                jump_cancel = true;
                rigidbody.gravityScale = gravityScale;
                can_multi_jump = true;
            }
            if (Input.GetKeyDown(KeyCode.LeftShift) && dash_cooldown_timer <= 0) {
                dash = true;
                dash_timer = dash_time;
                rigidbody.gravityScale = 0;
                dashRight = facingRight;
            }

            if (Input.GetButtonDown("Jump") && wallJump) {
                rigidbody.velocity = new Vector2(leftWallTouch ? 3 : -3, jumpSpeed + 2);
                wallJump = false;
                leftWallTouch = false;
                wallJumpTimer = .25f;
                jump_count = max_jump_count - 1;
            }

        } 
        else {

            hit_timer -= Time.deltaTime;
            if (hit_timer <= 0) {
                hit = false;
            }
        }


    }

    private void FixedUpdate() {
        float h;
      
        h = Input.GetAxisRaw("Horizontal");
        
     
        

        if (facingRight && h == -1) {
            facingRight = false;
        }

        if (!facingRight && h == 1) {
            facingRight = true;
        }

        //Handle dashing and regular movement
        if (dash) {
            dash_timer -= Time.deltaTime;
            if (dash_timer <= 0) {
                dash = false;
                dash_cooldown_timer = dash_cooldown;
                rigidbody.gravityScale = gravityScale;

            }
            if (h == 0) {
                if (facingRight) {
                    h = 1;
                }
                else {
                    h = -1;
                }
            }
            rigidbody.velocity = Vector2.zero;
            rigidbody.AddForce(new Vector2(20 * (dashRight ? 1 : -1), 0), ForceMode2D.Impulse);


        }
        else if (wallJump && rigidbody.velocity.y <= 0) {
            rigidbody.velocity = new Vector2(0, -2);

        }
        else if (wallJumpTimer >= 0) {
            wallJumpTimer -= Time.deltaTime;
        }
        else if (!hit){
            rigidbody.velocity = new Vector2(maxSpeed * h, rigidbody.velocity.y);
        }


        if (jump) {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpSpeed);
            jump = false;

        }


        if (jump_cancel) {
            jump_cancel = false;
            if (rigidbody.velocity.y > jumpShortSpeed) {
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, jumpShortSpeed);
            }
        }


        if (knockback) {
            knockback = false;
            rigidbody.velocity = Vector2.zero;
            rigidbody.AddForce(new Vector2(15 * hit_position.x,2+(10 *hit_position.y)),ForceMode2D.Impulse);
           
        }


    }


    private void OnCollisionEnter2D(Collision2D collision) {

        checkCollisions(collision);


    }

    private void checkCollisions(Collision2D collision) {
        if (collision.gameObject.tag == "MovingPlatform") {
            if (is_grounded) {
                transform.parent = collision.transform;
            }
        }

        if (collision.gameObject.tag == "Enemy" && !hit) {

            if (immune_timer <= 0) {
                hit = true;
                immune_timer = immune_timer_reset;
                hit_timer = hit_reset_timer;
                knockback = true;
                hit_position.x = rigidbody.position.x - collision.rigidbody.position.x;
                hit_position.y = rigidbody.position.y - collision.rigidbody.position.y;
            }


        }
    }

    private void OnCollisionStay2D(Collision2D collision) {
        checkCollisions(collision);
       

    }
    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.tag == "MovingPlatform") {
            transform.parent = null;
        }
    }



}
